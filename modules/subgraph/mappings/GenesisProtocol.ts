import 'allocator/arena'
export { allocate_memory }

import { Entity, Value, store, crypto, ByteArray } from '@graphprotocol/graph-ts'

import {GPExecuteProposal, Stake, RedeemDaoBounty, RedeemReputation,NewProposal, ExecuteProposal, VoteProposal } from '../types/GenesisProtocol/GenesisProtocol'

function concat(a: ByteArray, b: ByteArray): ByteArray {
  let out = new Uint8Array(a.length + b.length)
  for (let i = 0; i < a.length; i++) {
    out[i] = a[i]
  }
  for (let j = 0; j < b.length; j++) {
    out[a.length + j] = b[j]
  }
  return out as ByteArray
}

export function handleNewProposal(event: NewProposal): void {
    let user = store.get('User', event.params._proposer.toHex())
    if (user == null) {
        user = new Entity()
        user.setAddress('id', event.params._proposer)
        store.set('User',event.params._proposer.toHex() , user as Entity)
    }

    let proposal = new Entity()
    proposal.setString('id', event.params._proposalId.toHex())
    proposal.setAddress('avatar', event.params._avatar)
    proposal.setAddress('proposer', event.params._proposer)

    store.set('Proposal', event.params._proposalId.toHex(), proposal)
}

export function handleVoteProposal(event: VoteProposal): void {
    let user = store.get('User', event.params._voter.toHex())
    if (user == null) {
        user = new Entity()
        user.setAddress('id', event.params._voter)
        store.set('User',event.params._voter.toHex() , user as Entity)
    }

    let vote = new Entity()
    let uniqueId = crypto.keccak256(concat(event.params._proposalId, event.params._voter)).toHex()

    vote.setString('proposal', event.params._proposalId.toHex())
    vote.setAddress('voter', event.params._voter)
    vote.setU256('vote', event.params._vote)

    store.set('Vote', uniqueId, vote)
}

export function handleStake(event: Stake): void {
    let user = store.get('User', event.params._staker.toHex())
    if (user == null) {
        user = new Entity()
        user.setAddress('id', event.params._staker)
        store.set('User',event.params._staker.toHex() , user as Entity)
    }

    let stake = new Entity()
    let uniqueId = crypto.keccak256(concat(event.params._proposalId, event.params._staker)).toHex()

    stake.setString('proposal', event.params._proposalId.toHex())
    stake.setAddress('staker', event.params._staker)
    stake.setU256('vote', event.params._vote)
    stake.setU256('stake', event.params._amount)

    store.set('Stake', uniqueId, stake)
}

export function handleGPExecuteProposal (event: GPExecuteProposal): void {
    let proposal = new Entity()
    proposal.setInt('state', event.params._executionState as u32)

    store.set('Proposal', event.params._proposalId.toHex(), proposal)

}

export function handleExecuteProposal(event: ExecuteProposal): void {
    let proposal = new Entity()
    proposal.setU256('decision', event.params._decision)

    store.set('Proposal', event.params._proposalId.toHex(), proposal)

}

export function handleRedeemDaoBounty (event: RedeemDaoBounty): void {
    let user = store.get('User', event.params._beneficiary.toHex())
    if (user == null) {
        user = new Entity()
        user.setAddress('id', event.params._beneficiary)
        store.set('User',event.params._beneficiary.toHex() , user as Entity)
    }

    let reward = new Entity()
    let uniqueId = crypto.keccak256(concat(event.params._proposalId, concat(event.params._beneficiary, event.params._amount as ByteArray))).toHex()

    reward.setAddress('beneficiary', event.params._beneficiary)
    reward.setU256('amount', event.params._amount)
    reward.setString('type', 'GEN')
    reward.setString('proposal', event.params._proposalId.toHex())

    store.set('Reward', uniqueId, reward)
}

export function handleRedeemReputation (event: RedeemReputation): void {
    let user = store.get('User', event.params._beneficiary.toHex())
    if (user == null) {
        user = new Entity()
        user.setAddress('id', event.params._beneficiary)
        store.set('User',event.params._beneficiary.toHex() , user as Entity)
    }

    let reward = new Entity()
    let uniqueId = crypto.keccak256(concat(event.params._proposalId, concat(event.params._beneficiary, event.params._amount as ByteArray))).toHex()

    reward.setAddress('beneficiary', event.params._beneficiary)
    reward.setU256('amount', event.params._amount)
    reward.setString('type', 'Rep')
    reward.setString('proposal', event.params._proposalId.toHex())

    store.set('Reward', uniqueId, reward)
}
