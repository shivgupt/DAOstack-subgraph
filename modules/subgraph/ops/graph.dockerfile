FROM graph-node:latest

# Install graph-cli
RUN apk add --update --no-cache bash curl jq make nodejs yarn
RUN yarn add @graphprotocol/graph-cli @graphprotocol/graph-ts

COPY subgraph.yaml subgraph.yaml
COPY schema.graphql schema.graphql
COPY tsconfig.json tsconfig.json
COPY mappings mappings
COPY abis abis
COPY types types
COPY dist dist
COPY ops/entry.sh entry.sh

ENTRYPOINT ["bash"]
CMD ["entry.sh"]
