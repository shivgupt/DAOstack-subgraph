FROM alpine:3.8

RUN mkdir -p /app
WORKDIR /app

# Build & Install graph-node
RUN apk add --no-cache --repository http://dl-cdn.alpinelinux.org/alpine/edge/community \
      cargo clang-dev git postgresql-dev &&\
    git clone https://github.com/graphprotocol/graph-node &&\
    cd graph-node &&\
    cargo install --path node &&\
    cd .. &&\
    rm -rf graph-node &&\
    mv /root/.cargo/bin/graph-node /usr/bin/graph-node &&\
    apk del cargo

ENTRYPOINT ["graph-node"]
