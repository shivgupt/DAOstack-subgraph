class GPExecuteProposal extends EthereumEvent {
  get params(): GPExecuteProposalParams {
    return new GPExecuteProposalParams(this);
  }
}

class GPExecuteProposalParams {
  _event: GPExecuteProposal;

  constructor(event: GPExecuteProposal) {
    this._event = event;
  }

  get _proposalId(): Bytes {
    return this._event.parameters[0].value.toBytes();
  }

  get _executionState(): u8 {
    return this._event.parameters[1].value.toU8();
  }
}

class Stake extends EthereumEvent {
  get params(): StakeParams {
    return new StakeParams(this);
  }
}

class StakeParams {
  _event: Stake;

  constructor(event: Stake) {
    this._event = event;
  }

  get _proposalId(): Bytes {
    return this._event.parameters[0].value.toBytes();
  }

  get _avatar(): Address {
    return this._event.parameters[1].value.toAddress();
  }

  get _staker(): Address {
    return this._event.parameters[2].value.toAddress();
  }

  get _vote(): U256 {
    return this._event.parameters[3].value.toU256();
  }

  get _amount(): U256 {
    return this._event.parameters[4].value.toU256();
  }
}

class Redeem extends EthereumEvent {
  get params(): RedeemParams {
    return new RedeemParams(this);
  }
}

class RedeemParams {
  _event: Redeem;

  constructor(event: Redeem) {
    this._event = event;
  }

  get _proposalId(): Bytes {
    return this._event.parameters[0].value.toBytes();
  }

  get _avatar(): Address {
    return this._event.parameters[1].value.toAddress();
  }

  get _beneficiary(): Address {
    return this._event.parameters[2].value.toAddress();
  }

  get _amount(): U256 {
    return this._event.parameters[3].value.toU256();
  }
}

class RedeemDaoBounty extends EthereumEvent {
  get params(): RedeemDaoBountyParams {
    return new RedeemDaoBountyParams(this);
  }
}

class RedeemDaoBountyParams {
  _event: RedeemDaoBounty;

  constructor(event: RedeemDaoBounty) {
    this._event = event;
  }

  get _proposalId(): Bytes {
    return this._event.parameters[0].value.toBytes();
  }

  get _avatar(): Address {
    return this._event.parameters[1].value.toAddress();
  }

  get _beneficiary(): Address {
    return this._event.parameters[2].value.toAddress();
  }

  get _amount(): U256 {
    return this._event.parameters[3].value.toU256();
  }
}

class RedeemReputation extends EthereumEvent {
  get params(): RedeemReputationParams {
    return new RedeemReputationParams(this);
  }
}

class RedeemReputationParams {
  _event: RedeemReputation;

  constructor(event: RedeemReputation) {
    this._event = event;
  }

  get _proposalId(): Bytes {
    return this._event.parameters[0].value.toBytes();
  }

  get _avatar(): Address {
    return this._event.parameters[1].value.toAddress();
  }

  get _beneficiary(): Address {
    return this._event.parameters[2].value.toAddress();
  }

  get _amount(): U256 {
    return this._event.parameters[3].value.toU256();
  }
}

class OwnershipRenounced extends EthereumEvent {
  get params(): OwnershipRenouncedParams {
    return new OwnershipRenouncedParams(this);
  }
}

class OwnershipRenouncedParams {
  _event: OwnershipRenounced;

  constructor(event: OwnershipRenounced) {
    this._event = event;
  }

  get previousOwner(): Address {
    return this._event.parameters[0].value.toAddress();
  }
}

class OwnershipTransferred extends EthereumEvent {
  get params(): OwnershipTransferredParams {
    return new OwnershipTransferredParams(this);
  }
}

class OwnershipTransferredParams {
  _event: OwnershipTransferred;

  constructor(event: OwnershipTransferred) {
    this._event = event;
  }

  get previousOwner(): Address {
    return this._event.parameters[0].value.toAddress();
  }

  get newOwner(): Address {
    return this._event.parameters[1].value.toAddress();
  }
}

class NewProposal extends EthereumEvent {
  get params(): NewProposalParams {
    return new NewProposalParams(this);
  }
}

class NewProposalParams {
  _event: NewProposal;

  constructor(event: NewProposal) {
    this._event = event;
  }

  get _proposalId(): Bytes {
    return this._event.parameters[0].value.toBytes();
  }

  get _avatar(): Address {
    return this._event.parameters[1].value.toAddress();
  }

  get _numOfChoices(): U256 {
    return this._event.parameters[2].value.toU256();
  }

  get _proposer(): Address {
    return this._event.parameters[3].value.toAddress();
  }

  get _paramsHash(): Bytes {
    return this._event.parameters[4].value.toBytes();
  }
}

class ExecuteProposal extends EthereumEvent {
  get params(): ExecuteProposalParams {
    return new ExecuteProposalParams(this);
  }
}

class ExecuteProposalParams {
  _event: ExecuteProposal;

  constructor(event: ExecuteProposal) {
    this._event = event;
  }

  get _proposalId(): Bytes {
    return this._event.parameters[0].value.toBytes();
  }

  get _avatar(): Address {
    return this._event.parameters[1].value.toAddress();
  }

  get _decision(): U256 {
    return this._event.parameters[2].value.toU256();
  }

  get _totalReputation(): U256 {
    return this._event.parameters[3].value.toU256();
  }
}

class VoteProposal extends EthereumEvent {
  get params(): VoteProposalParams {
    return new VoteProposalParams(this);
  }
}

class VoteProposalParams {
  _event: VoteProposal;

  constructor(event: VoteProposal) {
    this._event = event;
  }

  get _proposalId(): Bytes {
    return this._event.parameters[0].value.toBytes();
  }

  get _avatar(): Address {
    return this._event.parameters[1].value.toAddress();
  }

  get _voter(): Address {
    return this._event.parameters[2].value.toAddress();
  }

  get _vote(): U256 {
    return this._event.parameters[3].value.toU256();
  }

  get _reputation(): U256 {
    return this._event.parameters[4].value.toU256();
  }
}

class CancelProposal extends EthereumEvent {
  get params(): CancelProposalParams {
    return new CancelProposalParams(this);
  }
}

class CancelProposalParams {
  _event: CancelProposal;

  constructor(event: CancelProposal) {
    this._event = event;
  }

  get _proposalId(): Bytes {
    return this._event.parameters[0].value.toBytes();
  }

  get _avatar(): Address {
    return this._event.parameters[1].value.toAddress();
  }
}

class CancelVoting extends EthereumEvent {
  get params(): CancelVotingParams {
    return new CancelVotingParams(this);
  }
}

class CancelVotingParams {
  _event: CancelVoting;

  constructor(event: CancelVoting) {
    this._event = event;
  }

  get _proposalId(): Bytes {
    return this._event.parameters[0].value.toBytes();
  }

  get _avatar(): Address {
    return this._event.parameters[1].value.toAddress();
  }

  get _voter(): Address {
    return this._event.parameters[2].value.toAddress();
  }
}

class GenesisProtocol extends SmartContract {
  static bind(address: Address): GenesisProtocol {
    return new GenesisProtocol("GenesisProtocol", address);
  }
}
