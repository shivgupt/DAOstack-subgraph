
# get absolute paths to important dirs
cwd=$(shell pwd)
graph=$(cwd)/modules/subgraph
explorer=$(cwd)/modules/explorer

# Use the version number from package.json to tag prod docker images
version=$(shell cat package.json | grep "\"version\":" | grep -o "[0-9.]\+")

# Special variable: tells make here we should look for prerequisites
VPATH=ops:build:$(client)/build:$(contracts)/build:$(server)/build

find_options=-type f -not -path "*/node_modules/*" -not -name "*.swp"
#server_src=$(shell find $(server)/common $(server)/src $(find_options))
#client_src=$(shell find $(client)/src $(find_options))
#contract_src=$(shell find $(contracts)/contracts $(contracts)/migrations $(find_options))
#contract_ops=$(shell find $(contracts)/ops $(find_options))
graph_src=$(shell find $(graph) $(find_options))
explorer_src=$(shell find $(explorer)/src $(find_options))

app=alchemy
cacher_image=$(app)_cacher
#client_image=$(app)_client
#server_image=$(app)_server
#truffle_image=$(app)_truffle
graph_image=$(app)_graph
proxy_image=$(app)_proxy

# Make sure these directories exist
$(shell mkdir -p build $(explorer)/build)
me=$(shell whoami)

webpack=./node_modules/.bin/webpack
graph_cli=./node_modules/.bin/graph

builder_options=--name=buidler --tty --rm
	
####################
# Begin phony rules
.PHONY: default all dev prod clean

default: graph-image
all: dev prod
#dev: cacher-dev-image client-dev-image server-dev-image contract-artifacts
prod: proxy-image graph-image
clean:
	rm -rf build
	rm -rf $(explorer)/build

purge: clean

deploy: prod
	docker tag $(proxy_image):latest registry.gitlab.com/$(me)/alchemy:proxy
	docker tag $(graph_image):latest registry.gitlab.com/$(me)/alchemy:graph
	docker push registry.gitlab.com/$(me)/alchemy:proxy
	docker push registry.gitlab.com/$(me)/alchemy:graph

####################
# Begin Real Rules

# Proxy

proxy-image: ops/proxy.dockerfile ops/proxy.conf ops/proxy.entry.sh explorer.js
	docker build --file ops/proxy.dockerfile --tag $(proxy_image):latest .
	@touch build/proxy-image

# Explorer Client

explorer.js: explorer-builder $(explorer_src)
	docker run $(builder_options) \
	  --volume=$(explorer)/src:/app/src \
	  --volume=$(explorer)/public:/app/public \
	  --volume=$(explorer)/build:/app/build \
	  --entrypoint=yarn explorer_builder:dev build
	@touch build/explorer.js

explorer-builder: ops/builder.dockerfile $(explorer)/package.json
	docker build --file ops/builder.dockerfile --tag explorer_builder:dev $(explorer)
	@touch build/explorer-builder

# Build graph & graph node

graph-image: graph-node-image $(graph_src)
	docker build --file $(graph)/ops/graph.dockerfile --tag $(graph_image):latest $(graph)
	@touch build/graph-image

graph-node-image: $(graph)/ops/graph-node.dockerfile
	docker build --file $(graph)/ops/graph-node.dockerfile --tag graph-node:latest $(graph)
	@touch build/graph-node-image

graph: graph-types
	docker run $(builder_options) \
	  --volume=$(graph)/schema.graphql:/app/schema.graphql \
	  --volume=$(graph)/subgraph.yaml:/app/subgraph.yaml \
	  --volume=$(graph)/abis:/app/abis \
	  --volume=$(graph)/types:/app/types \
	  --volume=$(graph)/mappings:/app/mappings \
	  --volume=$(graph)/dist:/app/dist \
	  --entrypoint=$(graph_cli)  graph_builder:dev build subgraph.yaml
	@touch build/graph

graph-types: graph-builder
	docker run $(builder_options) \
	  --volume=$(graph)/subgraph.yaml:/app/subgraph.yaml \
	  --volume=$(graph)/schema.graphql:/app/schema.graphql \
	  --volume=$(graph)/mappings:/app/mappings \
	  --volume=$(graph)/abis:/app/abis \
	  --volume=$(graph)/tsconfig.json:/app/tsconfig.json \
	  --volume=$(graph)/types:/app/types \
	  --entrypoint=$(graph_cli) graph_builder:dev codegen --output-dir types subgraph.yaml
	@touch build/graph-types

graph-builder: $(graph)/package.json ops/builder.dockerfile
	docker build -f ops/builder.dockerfile -t graph_builder:dev $(graph)
	@touch build/graph-builder
