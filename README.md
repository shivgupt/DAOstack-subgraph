# DAOstack-subgraph

# Motivation

I recently submitted a proposal and was curious to know who all voted and staked on my proposal. Unfortunately Alchemy does not provide this feature. Any user of alchemy can see the total amount of stake and vote on any given proposal but for the details one has to go looking into the event log. Similarly, if I as a voter want to see the past proposals made by a given address, Alchemy does not offer an easy way.

By providing an easy way to explore history of voter/proposer/stake-holder we can help in DAO members making better decisions. A voter can refer to the quality of work delivered by the proposer in past (if any) to decide whether to vote again or not. Similarly a voter who voted for passing a bad proposal in past can be easily recognized by the other members.

This is an independent project rather than an extension to Alchemy because by changing the avatar address this project can start fetching information about other DAOs that share same of Alchemy's contracts. Also, this can be extended easily to handle any of the other contracts deployed in the DAOstack ecosystem.

# Agenda
- Phase 1: Get the backend setup to fetch events we are interested in
    - Get the information emitted from the blockchain and store it in a database using Graph Protocol
    - Provide an API so clients without a web3 connection can access the data
    - Be able to query the interesting information like all the stakers for a given proposal or all the proposals by a given eth address etc
- Phase 2: Visualize the queried information in form of piecharts/graphs
    - Website frontend to enable point and click exploration of data like total stakes & stake distribution for a proposal etc
    - Visualization of the information into pretty graphs or pie-charts

## Example queries
- all stakes and stake holders for given proposal
- all votes on given proposal
- all proposals proposed by given proposer (ethAddress)
- all proposals voted by given voter
- all proposals staked by given staker
- all proposals for given avatar

- Let me know of any other interesting queries that you think would be useful

## Notes
 - GPexecutionState: { None, PreBoostedTimeOut, PreBoostedBarCrossed, BoostedTimeOut,BoostedBarCrossed }
 - ProposalState { None ,Closed, Executed, PreBoosted,Boosted,QuietEndingPeriod }

