#!/usr/bin/env bash

####################
# ENV VARS

ETH_PROVIDER="mainnet:http://eth.bohendo.com:8546"
ETH_NETWORK_ID="4447"
ETH_MNEMONIC="candy maple cake sugar pudding cream honey rich smooth crumble sweet treat"
SUBGRAPH_SECRET="graph-secret"
DOMAINNAME=localhost
EMAIL=noreply@example.com

####################

me=`whoami`
project=alchemy
proxy_image=registry.gitlab.com/$me/$project:proxy
graph_image=registry.gitlab.com/$me/$project:graph
database_image=postgres:10
ganache_image=trufflesuite/ganache-cli:latest
ipfs_image=ipfs/go-ipfs:latest

function pull_if_unavailable {
    if [[ -z "`docker image ls | grep ${1%:*} | grep ${1#*:}`" ]]
    then
        docker pull $1
    fi
}

pull_if_unavailable $database_image
pull_if_unavailable $ganache_image
pull_if_unavailable $ipfs_image
docker pull $proxy_image
docker pull $graph_image

function new_secret {
    secret=$2
    if [[ -z "$secret" ]]
    then
        secret=`head -c 32 /dev/urandom | xxd -plain -c 32 | tr -d '\n\r'`
    fi
    if [[ -z "`docker secret ls -f name=$1 | grep -w $1`" ]]
    then
        id=`echo $secret | tr -d '\n\r' | docker secret create $1 -`
        echo "Created secret called $1 with id $id"
    fi
}

new_secret graph_secret $SUBGRAPH_SECRET

mkdir -p /tmp/$project
cat - > /tmp/$project/docker-compose.yml <<EOF
version: '3.4'

secrets:
  graph_secret:
    external: true

volumes:
  letsencrypt:
  devcerts:
  graph_data:
  ipfs:

services:

  proxy:
    image: $proxy_image
    deploy:
      mode: global
    depends_on:
      - graph
    volumes:
      - letsencrypt:/etc/letsencrypt
      - devcerts:/etc/letsencrypt/devcerts
    environment:
      - DOMAINNAME=$DOMAINNAME
      - EMAIL=$EMAIL
    ports:
      - "80:80"
      - "443:443"

  graph:
    image: $graph_image
    ports:
      - '8000:8000'
      - '8001:8001'
      - '8020:8020'
    depends_on:
      - ipfs
      - database
      - ganache
    environment:
      postgres_host: database:5432
      postgres_user: $project
      postgres_db: $project
      postgres_pass: $SUBGRAPH_SECRET
      ipfs: ipfs:5001
      ethereum: $ETH_PROVIDER

  ipfs:
    image: $ipfs_image
    ports:
      - '5001:5001'
    volumes:
      - ipfs:/data/ipfs

  database:
    image: postgres:10
    deploy:
      mode: global
    secrets:
      - graph_secret
    environment:
      POSTGRES_USER: $project
      POSTGRES_DB: $project
      POSTGRES_PASSWORD_FILE: /run/secrets/graph_secret
    volumes:
      - graph_data:/var/lib/postgresql/data
EOF

docker stack deploy -c /tmp/$project/docker-compose.yml $project
rm -rf /tmp/$project

echo -n "Waiting for the $project stack to wake up."
number_of_services=4
while true
do
    sleep 3
    if [[ "`docker container ls | grep $project | wc -l | sed 's/ //g'`" == "$number_of_services" ]]
    then
        echo " Good Morning!"
        break
    else
        echo -n "."
    fi
done

